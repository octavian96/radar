# ----------------------------------------------------------------------------
# -                        Open3D: www.open3d.org                            -
# ----------------------------------------------------------------------------
# Copyright (c) 2018-2023 www.open3d.org
# SPDX-License-Identifier: MIT
# ----------------------------------------------------------------------------
import struct

# See https://github.com/isl-org/Open3D/issues/2869

import numpy as np
import open3d as o3d
import open3d.visualization.gui as gui
import open3d.visualization.rendering as rendering
import threading
import time
from serial import SerialTimeoutException
from serial.tools import list_ports
import serial
from enum import Enum
from collections import deque
import queue


class Flags(Enum):
    ZERO = 0b00000000
    GO = 0b00000001
    SCANNER = 0b00000010	# Radar is 0b00000000
    MOVE = 0b00000100	# Moves UP unless DOWN is specified
    DOWN = 0b00001000
    RESTART = 0b00010000
    FULL_MASK = 0b11111111

class AppWindow:

    def __init__(self, width, height, ports):
        self.serialPort = None
        self._bytesizes = {
            "EIGHTBITS": serial.EIGHTBITS,
            "SEVENBITS": serial.SEVENBITS,
            "SIXBITS": serial.SIXBITS,
            "FIVEBITS": serial.FIVEBITS,
        }
        self._parities = {
            "NONE": serial.PARITY_NONE,
            "EVEN": serial.PARITY_EVEN,
            "ODD": serial.PARITY_ODD,
            "MARK": serial.PARITY_MARK,
            "SPACE": serial.PARITY_SPACE
        }
        self._stopbits = {
            "ONE": serial.STOPBITS_ONE,
            "ONE_POINT_FIVE": serial.STOPBITS_ONE_POINT_FIVE,
            "TWO": serial.STOPBITS_TWO
        }
        self._stop_event = None

        self._pcd = o3d.geometry.PointCloud()
        self._pcd_name = "pointcloud"

        self.material = rendering.MaterialRecord()
        self.material.base_color = [1, 0, 0, 1]
        self.material.point_size = 5

        self._is_radar_mode = False

        self.window = gui.Application.instance.create_window(
            "USS", width, height)
        w = self.window  # to make the code more concise
        w.set_on_close(self._on_close)

        # 3D widget
        self._scene = gui.SceneWidget()
        self._scene.scene = rendering.Open3DScene(w.renderer)

        # Rather than specifying sizes in pixels, which may vary in size based
        # on the monitor, especially on macOS which has 220 dpi monitors, use
        # the em-size. This way sizings will be proportional to the font size,
        # which will create a more visually consistent size across platforms.
        self._em = w.theme.font_size

        # Widgets are laid out in layouts: gui.Horiz, gui.Vert,
        # gui.CollapsableVert, and gui.VGrid. By nesting the layouts we can
        # achieve complex designs. Usually we use a vertical layout as the
        # topmost widget, since widgets tend to be organized from top to bottom.
        # Within that, we usually have a series of horizontal layouts for each
        # row. All layouts take a spacing parameter, which is the spacing
        # between items in the widget, and a margins parameter, which specifies
        # the spacing of the left, top, right, bottom margins (This acts like
        # the 'padding' property in CSS).
        # The component WidgetProxy acts as a normal Widget but its content can be
        # set and changed to display different actual Widgets.
        self._panel = gui.WidgetProxy()

        self._panel.set_widget(self._setup_port_widget(ports))
        # ----

        # Normally our user interface can be children of all one layout (usually
        # a vertical layout), which is then the only child of the window. In our
        # case we want the scene to take up all the space and the settings panel
        # to go above it. We can do this custom layout by providing an on_layout
        # callback. The on_layout callback should set the frame
        # (position + size) of every child correctly. After the callback is
        # done the window will layout the grandchildren.
        w.add_child(self._scene)
        w.add_child(self._panel)
        w.set_on_layout(self._on_layout)

        self._apply_settings()

    def _on_close(self):
        flags = [Flags.FULL_MASK]
        self._send_command(flags)
        self._stop_event.set()
        self._serialPort.close()
        return True

    def _send_command(self, flags):
        print("Sending command")
        # (Flags.SEND.value | Flags.RADAR.value).to_bytes(byteorder='little', length=1)
        print(flags)
        command = Flags.ZERO.value
        for flag in flags:
            command |= flag.value
        # if len(flags) > 1:
        #    flag = reduce(lambda x, y: x.value | y.value, flags)
        try:
            print(
                f"Sending {command} - {command.to_bytes(byteorder='little', length=1)}")
            written = self._serialPort.write((command,))
            corr = written == len(command.to_bytes(
                byteorder='little', length=1))
            return corr
        except SerialTimeoutException:
            print(f"Timeout reached while writing flag {command}")
            return False

    def _ser_read(self):
        val = deque(
            [int.to_bytes(0x0, byteorder='little', length=1)] * 8, maxlen=8)
        nums = deque(maxlen=12)
        control = int.to_bytes(
            0xFFFFFFFFFFFFFFFF, byteorder='little', length=8)
        print("Started reading")
        while not self._stop_event.is_set():
            if self._serialPort.in_waiting:
                try:
                    new_byte = self._serialPort.read()
                    #print(f"Read {new_byte}")
                    #print(f"Received {new_byte} - {int.from_bytes(new_byte, byteorder='little')}")
                    val.append(new_byte)
                    nums.append(new_byte)
                    bb = b''.join(list(val))
                    # print(f"Bytes: {bytes_list} - {bb}")
                    if bb == control:
                        #print(f"Control! len(nums) = {len(nums)}")
                        if len(nums) == 12:
                            coords = list(nums)[:-8]
                            pan = int.from_bytes(
                                coords.pop(0), byteorder='little')
                            tilt = int.from_bytes(
                                coords.pop(0), byteorder='little')
                            distance = int.from_bytes(
                                b''.join(coords), byteorder='little')
                            #print(f"Adding ({pan}, {tilt}, {distance})...")
                            print(f"Distance: {distance:07d}")
                            self.add_point((pan, tilt, distance))
                            #print("Added")
                        nums.clear()
                except Exception as ex:
                    print(ex)
            else:
                time.sleep(0.05)
        print("Stopped reading")

    def _start_thread(self, function, args=()):
        threading.Thread(target=function, args=args, daemon=True).start()
        print("Thread started")

    def _on_device_choice(self, name, index):
        self._device_choice = name
        print(f"Set device to {self._device_choice}")

    def _on_baudrate_choice(self, name, index):
        self._baudrate_choice = int(name)
        print(f"Set baudrate to {self._baudrate_choice}")

    def _on_bytesize_choice(self, name, index):
        self._bytesize_choice = self._bytesizes[name] if name in self._bytesizes else list(
            self._bytesizes.values())[0]
        print(f"Set bytesize to {self._bytesize_choice}")

    def _on_parity_choice(self, name, index):
        self._parity_choice = self._parities[name] if name in self._parities else list(
            self._parities.values())[0]
        print(f"Set parity to {self._parity_choice}")

    def _on_stopbits_choice(self, name, index):
        self._stopbits_choice = self._stopbits[name] if name in self._stopbits else list(
            self._stopbits.values())[0]
        print(f"Set stopbits to {self._stopbits_choice}")

    def _choose_operation_mode(self):
        # Show a simple dialog. Although the Dialog is actually a widget, you can
        # treat it similar to a Window for layout and put all the widgets in a
        # layout which you make the only child of the Dialog.
        dlg = gui.Dialog("Operation mode")

        # Add the text
        dlg_layout = gui.Vert(self._em, gui.Margins(
            self._em, self._em, self._em, self._em))
        dlg_layout.add_child(gui.Label(f"Choose the sensor operating mode:"))

        # Add the Ok button. We need to define a callback function to handle
        # the click.
        scanner = gui.Button("Scanner")
        scanner.set_on_clicked(
            lambda: self.window.close_dialog() or self._scanner() or self._panel.set_widget(self._setup_start_widget()))
        radar = gui.Button("Radar")
        radar.set_on_clicked(
            lambda: self.window.close_dialog() or self._radar() or self._panel.set_widget(self._setup_start_widget()))

        # We want the Ok button to be an the right side, so we need to add
        # a stretch item to the layout, otherwise the button will be the size
        # of the entire row. A stretch item takes up as much space as it can,
        # which forces the button to be its minimum size.
        h = gui.Horiz()
        h.add_stretch()
        h.add_child(scanner)
        h.add_stretch()
        h.add_child(radar)
        h.add_stretch()
        dlg_layout.add_child(h)

        dlg.add_child(dlg_layout)
        self.window.show_dialog(dlg)

    def _on_button_connect(self):
        print("Connect button pressed")
        try:
            serial_port = serial.Serial(
                port=self._device_choice,
                baudrate=self._baudrate_choice,
                bytesize=self._bytesize_choice,
                stopbits=self._stopbits_choice,
                xonxoff=False,
                timeout=None,
                write_timeout=0
            )
        except (ValueError, serial.SerialException) as ex:
            self.window.show_message_box(
                "Error opening serial port", f"Could not open the serial port {self._device_choice}.\nError: {ex}")
        else:
            if not serial_port.is_open:
                self.window.show_message_box(
                    "Error opening serial port", f"Could not open the serial port {self._device_choice}.")
            else:
                self._serialPort = serial_port
                self._panel.set_widget(None)
                self._choose_operation_mode()

    def _setup_port_widget(self, ports):
        separation_height = int(round(0.5 * self._em))
        vis = gui.Vert(0.25 * self._em,
                       gui.Margins(2 * self._em, 0, 2 * self._em, 0))
        vis.add_fixed(separation_height)
        vis.add_child(gui.Label("Serial port"))
        vis.add_fixed(separation_height)

        grid = gui.VGrid(2, 0.25 * self._em,
                         gui.Margins(self._em, 0, 2 * self._em, 0))

        combobox_device_choice = gui.Combobox()
        for device in ports:
            combobox_device_choice.add_item(device)
        combobox_device_choice.set_on_selection_changed(self._on_device_choice)
        grid.add_child(gui.Label("Device"))
        grid.add_child(combobox_device_choice)
        self._on_device_choice(combobox_device_choice.get_item(0), 0)

        combobox_baudrate_choice = gui.Combobox()
        baudrates = (str(x) for x in [115200, 57600, 38400, 19200, 9600,
                     4800, 2400, 1800, 1200, 600, 300, 200, 150, 134, 110, 75, 50])
        for baudrate in baudrates:
            combobox_baudrate_choice.add_item(baudrate)
        combobox_baudrate_choice.set_on_selection_changed(
            self._on_baudrate_choice)
        grid.add_child(gui.Label("Baudrate"))
        grid.add_child(combobox_baudrate_choice)
        self._on_baudrate_choice(combobox_baudrate_choice.get_item(0), 0)

        combobox_bytesize_choice = gui.Combobox()
        for bytesize in self._bytesizes.keys():
            combobox_bytesize_choice.add_item(bytesize)
        combobox_bytesize_choice.set_on_selection_changed(
            self._on_bytesize_choice)
        grid.add_child(gui.Label("Bytesize"))
        grid.add_child(combobox_bytesize_choice)
        self._on_bytesize_choice(combobox_bytesize_choice.get_item(0), 0)

        combobox_parity_choice = gui.Combobox()
        for parity in self._parities.keys():
            combobox_parity_choice.add_item(parity)
        combobox_parity_choice.set_on_selection_changed(self._on_parity_choice)
        grid.add_child(gui.Label("Parity"))
        grid.add_child(combobox_parity_choice)
        self._on_parity_choice(combobox_parity_choice.get_item(0), 0)

        combobox_stopbits_choice = gui.Combobox()
        for stopbits in self._stopbits.keys():
            combobox_stopbits_choice.add_item(stopbits)
        combobox_stopbits_choice.set_on_selection_changed(
            self._on_stopbits_choice)
        grid.add_child(gui.Label("Stopbits"))
        grid.add_child(combobox_stopbits_choice)
        self._on_stopbits_choice(combobox_stopbits_choice.get_item(0), 0)

        vis.add_child(grid)

        button_connect = gui.Button("Connect")
        button_connect.set_on_clicked(self._on_button_connect)

        h = gui.Horiz()
        h.add_stretch()
        h.add_child(button_connect)
        h.add_stretch()
        vis.add_fixed(separation_height)
        vis.add_child(h)
        vis.add_fixed(separation_height)

        return vis

    def _apply_settings(self):
        #print("_apply_settings")
        bg_color = gui.Color(1, 1, 1)
        bg_color = [
            bg_color.red, bg_color.green,
            bg_color.blue, bg_color.alpha
        ]
        self._scene.scene.set_background(bg_color)
        self._scene.scene.show_skybox(False)
        self._scene.scene.show_axes(False)
        self._scene.scene.scene.enable_indirect_light(True)
        self._scene.scene.scene.set_indirect_light_intensity(45000)
        sun_color = gui.Color(1, 1, 1)
        sun_color = [
            sun_color.red,
            sun_color.green,
            sun_color.blue
        ]
        sun_dir = [0.577, -0.577, -0.577]
        sun_intensity = 45000
        self._scene.scene.scene.set_sun_light(
            sun_dir, sun_color, sun_intensity)
        self._scene.scene.scene.enable_sun_light(True)

        self._scene.scene.update_material(self.material)

    def _on_layout(self, layout_context):
        #print("_on_layout")
        # The on_layout callback should set the frame (position + size) of every
        # child correctly. After the callback is done the window will layout
        # the grandchildren.
        r = self.window.content_rect
        self._scene.frame = r
        width = 17 * layout_context.theme.font_size
        height = min(
            r.height,
            self._panel.calc_preferred_size(
                layout_context, gui.WidgetProxy.Constraints()).height)
        self._panel.frame = gui.Rect(r.get_right() - width, r.y, width,
                                     height)

    def add_points(self, points):
        pass

    def add_point(self, point):
        pass

    def _on_button_start(self):
        print("Starting...")
        flags = [Flags.GO, Flags.RESTART]
        if not self._is_radar_mode:
            flags.append(Flags.SCANNER)
        # else:
        #    flags.append(Flags.CARTESIAN)
        sent = self._send_command(flags)
        if sent:
            self._panel.set_widget(self._setup_buttons_widget())
            if self._stop_event == None:
                self._stop_event = threading.Event()
                self._start_thread(self._ser_read)
        else:
            dlg = gui.Dialog("Error")
            # Add the text
            dlg_layout = gui.Vert(self._em, gui.Margins(
                self._em, self._em, self._em, self._em))
            dlg_layout.add_child(gui.Label(f"Could not send command. Retry."))

            # Add the Ok button. We need to define a callback function to handle
            # the click.
            ok = gui.Button("OK")
            ok.set_on_clicked(lambda: self.window.close_dialog())

            h = gui.Horiz()
            h.add_stretch()
            h.add_child(ok)
            h.add_stretch()
            dlg_layout.add_child(h)

            dlg.add_child(dlg_layout)
            self.window.show_dialog(dlg)

    def _setup_start_widget(self):
        separation_height = int(round(0.5 * self._em))
        vis = gui.Vert(
            0.25 * self._em,
            gui.Margins(2 * self._em, 0, 2 * self._em, 0)
        )
        vis.add_fixed(separation_height)
        h = gui.Horiz()
        h.add_stretch()
        h.add_child(gui.Label("Commands"))
        h.add_stretch()
        vis.add_child(h)
        vis.add_fixed(separation_height)

        button_start = gui.Button("Start")
        button_start.set_on_clicked(self._on_button_start)
        h = gui.Horiz()
        h.add_stretch()
        h.add_child(button_start)
        h.add_stretch()
        vis.add_child(h)
        vis.add_fixed(separation_height)

        if not self._is_radar_mode:
            button_save = gui.Button("Save PointCloud")
            button_save.set_on_clicked(lambda: self._save_pcd(self._pcd))
            h = gui.Horiz()
            h.add_stretch()
            h.add_child(button_save)
            h.add_stretch()
            vis.add_child(h)
            vis.add_fixed(separation_height)

        return vis

    def _on_button_resume(self):
        print("Resuming...")
        flags = [Flags.GO]
        if not self._is_radar_mode:
            flags.append(Flags.SCANNER)
        # else:
        #    flags.append(Flags.CARTESIAN)
        sent = self._send_command(flags)
        if sent:
            self._panel.set_widget(self._setup_buttons_widget())
        else:
            dlg = gui.Dialog("Error")

            # Add the text
            dlg_layout = gui.Vert(self._em, gui.Margins(
                self._em, self._em, self._em, self._em))
            dlg_layout.add_child(gui.Label(f"Could not send command. Retry."))

            # Add the Ok button. We need to define a callback function to handle
            # the click.
            ok = gui.Button("OK")
            ok.set_on_clicked(lambda: self.window.close_dialog())

            h = gui.Horiz()
            h.add_stretch()
            h.add_child(ok)
            h.add_stretch()
            dlg_layout.add_child(h)

            dlg.add_child(dlg_layout)
            self.window.show_dialog(dlg)

    def _on_button_pause(self):
        print("Pausing...")
        sent = self._send_command([Flags.ZERO])
        if sent:
            self._panel.set_widget(self._setup_buttons_widget(paused=True))
        else:
            dlg = gui.Dialog("Error")

            # Add the text
            dlg_layout = gui.Vert(self._em, gui.Margins(
                self._em, self._em, self._em, self._em))
            dlg_layout.add_child(gui.Label(f"Could not send command. Retry."))

            # Add the Ok button. We need to define a callback function to handle
            # the click.
            ok = gui.Button("OK")
            ok.set_on_clicked(lambda: self.window.close_dialog())

            h = gui.Horiz()
            h.add_stretch()
            h.add_child(ok)
            h.add_stretch()
            dlg_layout.add_child(h)

            dlg.add_child(dlg_layout)
            self.window.show_dialog(dlg)

    def _on_button_stop(self):
        print("Stopping...")
        sent = self._send_command([Flags.RESTART])
        if sent:
            self._panel.set_widget(self._setup_start_widget())
        else:
            dlg = gui.Dialog("Error")

            # Add the text
            dlg_layout = gui.Vert(self._em, gui.Margins(
                self._em, self._em, self._em, self._em))
            dlg_layout.add_child(gui.Label(f"Could not send command. Retry."))

            # Add the Ok button. We need to define a callback function to handle
            # the click.
            ok = gui.Button("OK")
            ok.set_on_clicked(lambda: self.window.close_dialog())

            h = gui.Horiz()
            h.add_stretch()
            h.add_child(ok)
            h.add_stretch()
            dlg_layout.add_child(h)

            dlg.add_child(dlg_layout)
            self.window.show_dialog(dlg)

    def _on_button_up(self):
        print("Moving up...")
        sent = self._send_command([Flags.MOVE, Flags.GO])
        if not sent:
            dlg = gui.Dialog("Error")

            # Add the text
            dlg_layout = gui.Vert(self._em, gui.Margins(
                self._em, self._em, self._em, self._em))
            dlg_layout.add_child(gui.Label(f"Could not send command. Retry."))

            # Add the Ok button. We need to define a callback function to handle
            # the click.
            ok = gui.Button("OK")
            ok.set_on_clicked(lambda: self.window.close_dialog())

            h = gui.Horiz()
            h.add_stretch()
            h.add_child(ok)
            h.add_stretch()
            dlg_layout.add_child(h)

            dlg.add_child(dlg_layout)
            self.window.show_dialog(dlg)

    def _on_button_down(self):
        print("Moving up...")
        sent = self._send_command([Flags.MOVE, Flags.DOWN, Flags.GO])
        if not sent:
            dlg = gui.Dialog("Error")

            # Add the text
            dlg_layout = gui.Vert(self._em, gui.Margins(
                self._em, self._em, self._em, self._em))
            dlg_layout.add_child(gui.Label(f"Could not send command. Retry."))

            # Add the Ok button. We need to define a callback function to handle
            # the click.
            ok = gui.Button("OK")
            ok.set_on_clicked(lambda: self.window.close_dialog())

            h = gui.Horiz()
            h.add_stretch()
            h.add_child(ok)
            h.add_stretch()
            dlg_layout.add_child(h)

            dlg.add_child(dlg_layout)
            self.window.show_dialog(dlg)

    def _setup_buttons_widget(self, paused=False):
        separation_height = int(round(0.5 * self._em))
        vis = gui.Vert(
            0.25 * self._em,
            gui.Margins(2 * self._em, 0, 2 * self._em, 0)
        )
        vis.add_fixed(separation_height)
        h = gui.Horiz()
        h.add_stretch()
        h.add_child(gui.Label("Commands"))
        h.add_stretch()
        vis.add_child(h)
        vis.add_fixed(separation_height)

        if self._is_radar_mode:
            button_up = gui.Button("Up")
            button_up.set_on_clicked(self._on_button_up)
            button_down = gui.Button("Down")
            button_down.set_on_clicked(self._on_button_down)
            h = gui.Vert()
            h.add_stretch()
            h.add_child(button_up)
            h.add_stretch()
            h.add_child(button_down)
            h.add_stretch()
            vis.add_child(h)
            vis.add_fixed(separation_height)


        if paused:
            button = gui.Button("Resume")
            button.set_on_clicked(self._on_button_resume)
        else:
            button = gui.Button("Pause")
            button.set_on_clicked(self._on_button_pause)
        button_stop = gui.Button("Stop")
        button_stop.set_on_clicked(self._on_button_stop)
        h = gui.Horiz()
        h.add_stretch()
        h.add_child(button)
        h.add_stretch()
        h.add_child(button_stop)
        h.add_stretch()
        vis.add_child(h)
        vis.add_fixed(separation_height)

        return vis

    def _center_camera(self, z_position=None):
        # See https://stackoverflow.com/a/75519461
        m = self._scene.scene.camera.get_model_matrix()
        m = np.identity(4)
        rot = m[:3, :3]
        pos = m[:3, 3] + np.asarray([0, 2, 0])
        pos[2] = z_position if z_position else 14
        camera_dir = np.dot(rot, np.asarray([[0], [0], [-1]])).squeeze()
        # cylinder.paint_uniform_color(np.array([20, 100, 255])/255)
        self._scene.scene.camera.look_at(
            pos + camera_dir, pos, np.asarray([0, 1, 0]))

    def _test_add_points_scanner(self):
        time.sleep(5)
        print("Start sending points...")
        while True:
            points = np.random.rand(
                500, 3) * np.asarray([8, 4, 4]) + np.asarray([-4, 0, 0])
            try:
                self.add_points(points)
            except Exception as ex:
                print(ex)
            time.sleep(2)

    def _save_pcd(self, pcd):
        dlg = gui.FileDialog(gui.FileDialog.SAVE,
                             "Save PointCloud", self.window.theme)
        dlg.add_filter(".pcd", "PointCloud")
        dlg.set_on_cancel(lambda: self.window.close_dialog())

        def save_routine(path):
            try:
                o3d.io.write_point_cloud(path, pcd)
            except Exception as ex:
                print(ex)

        dlg.set_on_done(lambda name: self.window.close_dialog()
                        or save_routine(name))

        self.window.show_dialog(dlg)

    def _clamp(self, n, smallest, largest): return max(
        smallest, min(n, largest))

    def _scanner(self):
        self._scene.scene.add_geometry(
            self._pcd_name, self._pcd, self.material)

        def add_point_scanner(point):
            add_points_scanner([point])

        def add_points_scanner(points):
            #print(f"Adding scanner points")
            #for pan, tilt, distance in points:
            #    print(f"pan: {pan}, tilt: {tilt}, distance: {distance}")

            points = map(
                lambda point: (
                    self._clamp(point[0], 0, 180),
                    self._clamp(point[1], 0, 180),
                    self._clamp(point[2]/500, 0, 6)
                ),
                points
            )
            points = list(map(lambda point: self._cartesian(*point), points))

            #for x, y, z in points:
            #    print(f"x: {x}, y: {y}, z: {z}")

            self._pcd.points.extend(points)

            def update_points():
                self._scene.scene.remove_geometry(self._pcd_name)
                self._scene.scene.add_geometry(
                    self._pcd_name, self._pcd, self.material)

            o3d.visualization.gui.Application.instance.post_to_main_thread(
                self.window, update_points)

        # TODO: remove this in production
        # self._start_thread(self._test_add_points_scanner)

        self.add_points = add_points_scanner
        self.add_point = add_point_scanner

        self._center_camera()

        # See https://github.com/isl-org/Open3D/issues/2320
        self._scene.set_on_key(
            lambda e:
            (e.key == gui.KeyName.SPACE and self._center_camera() and False) or
            o3d.visualization.gui.SceneWidget.EventCallbackResult.HANDLED
        )

    def _test_add_points_radar(self):
        print("Start sending points...")
        while True:
            points = np.random.rand(1, 3) * np.asarray([180, 0, 4])
            print(f"Generated {points}")
            try:
                self.add_points(points)
            except Exception as ex:
                print(ex)
            time.sleep(0.5)

    def _crop_above_zero(self, geometry):
        max_b = geometry.get_max_bound()
        min_b = geometry.get_min_bound()
        min_b[1] = 0
        # print(f"Min: {min_b}, max: {max_b}")
        return geometry.crop(
            o3d.geometry.AxisAlignedBoundingBox(
                min_b,
                max_b
            )
        )

    def _remove_central_cylinder(self, geometry, center=(0, 0, 0), radius=1.0):
        def triangle_to_points(triangle):
            return np.asarray(
                (geometry.vertices[triangle[0]], geometry.vertices[triangle[1]], geometry.vertices[triangle[2]]))

        print(f"Pre: {len(geometry.triangles)}")
        # trs = map(lambda tr: np.array((geometry.vertices[tr[0]], geometry.vertices[tr[1]], geometry.vertices[tr[2]])), geometry.triangles)
        trs = filter(
            lambda tr:
            np.sqrt(np.sum(triangle_to_points(tr)[0] * triangle_to_points(tr)[0])) >= radius or
            np.sqrt(np.sum(triangle_to_points(tr)[1] * triangle_to_points(tr)[1])) >= radius or
            np.sqrt(np.sum(triangle_to_points(tr)[
                2] * triangle_to_points(tr)[2])) >= radius,
            geometry.triangles
        )
        geometry.triangles = o3d.utility.Vector3iVector(trs)
        print(f"Post: {len(geometry.triangles)}")

        return geometry

    def _rotating_bar(self, radius):
        points = [
            [0, 0, .45],
            [-radius, 0, .45],
        ]
        lines = [
            [0, 1],
        ]
        colors = [np.asarray((27, 140, 8)) / 255 for i in range(len(lines))]
        bar_name = "bar"
        bar = o3d.geometry.LineSet(
            points=o3d.utility.Vector3dVector(points),
            lines=o3d.utility.Vector2iVector(lines),
        )
        bar.colors = o3d.utility.Vector3dVector(colors)

        # r = bar.get_rotation_matrix_from_xyz((0, 0, -np.pi / 2))
        # bar.rotate(r, center=(0, 0, 0.5))

        mat = rendering.MaterialRecord()
        mat.shader = "unlitLine"
        mat.line_width = 5  # note that this is scaled with respect to pixels,
        # so will give different results depending on the
        # scaling values of your system

        self._scene.scene.add_geometry(bar_name, bar, mat)
        # o3d.visualization.gui.Application.instance.post_to_main_thread(
        #        self.window, self._scene.scene.add_geometry(bar_name, bar, self.settings._materials[Settings.DEPTH]))
        i = 0
        div = 90
        while True:
            time.sleep(0.05)
            # Every "div" steps change direction
            mult = (int(i / div) % 2) * 2 - 1
            r = bar.get_rotation_matrix_from_xyz((0, 0, mult * np.pi / div))
            bar.rotate(r, center=(0, 0, 0.5))

            def update_bar():
                self._scene.scene.remove_geometry(bar_name)
                self._scene.scene.add_geometry(bar_name, bar, mat)

            o3d.visualization.gui.Application.instance.post_to_main_thread(
                self.window, update_bar)
            i += 1

    def _radar_grid(self, radius):

        n = 15  # points to generate
        points = [(0, 0, .45)] + [
            [
                radius * np.cos(theta),
                radius * np.sin(theta),
                .45
            ] for theta in (np.pi * i / n for i in range(n))
        ]
        lines = [[0, index] for index in range(len(points))][1:]
        colors = [np.asarray((15, 102, 1)) / 255 for i in range(len(lines))]
        grid_name = "grid"
        grid = o3d.geometry.LineSet(
            points=o3d.utility.Vector3dVector(points),
            lines=o3d.utility.Vector2iVector(lines),
        )
        grid.colors = o3d.utility.Vector3dVector(colors)

        mat = rendering.MaterialRecord()
        mat.shader = "unlitLine"
        mat.line_width = 1  # note that this is scaled with respect to pixels,
        # so will give different results depending on the
        # scaling values of your system

        self._scene.scene.add_geometry(grid_name, grid, mat)

    def _cartesian(self, azimuth, elevation, radius):
        x = radius * np.cos(np.deg2rad(elevation)) * np.cos(np.deg2rad(azimuth))
        y = radius * np.cos(np.deg2rad(elevation)) * np.sin(np.deg2rad(azimuth))
        z = radius * np.sin(np.deg2rad(elevation))
        return x, y, z

    def _rect(self, theta, r):

        x = r * np.cos(np.deg2rad(theta))
        y = r * np.sin(np.deg2rad(theta))
        return x, y

    def _radar(self):
        # Disable mouse interaction
        # self._scene.set_on_mouse(
        #     lambda event: o3d.visualization.gui.SceneWidget.EventCallbackResult.CONSUMED)
        print("Disabled mouse interaction")

        self._is_radar_mode = True

        # Set constants for drawing almost flat cylinders
        line_width = 0.04
        space_width = 1.96
        initial_height = 0.9
        space_material = rendering.MaterialRecord()
        # space_material.base_color = np.asarray([20, 100, 255, 255]) / 255
        space_material.base_color = np.asarray([0, 0, 0, 0]) / 255
        line_material = rendering.MaterialRecord()
        # line_material.base_color = np.asarray([255, 255, 255, 255]) / 255
        line_material.base_color = np.asarray([35, 171, 14, 255]) / 255

        # Draw concentric cylinders (with decreasing height so that they don't cover each other when rendered
        height = initial_height
        radius = -line_width / 2
        cylinders = [(line_width, line_material), (space_width,
                                                   space_material)] * 6 + [(line_width / 2, line_material)]
        for width, material in cylinders:
            height -= 0.01
            radius += width
            cyl = o3d.geometry.TriangleMesh.create_cylinder(
                radius=radius, height=height, resolution=60)
            cyl = self._crop_above_zero(cyl)
            self._scene.scene.add_geometry(f"cylinder_{height}", cyl, material)
        print("Radar drawn")

        # Draw the grid
        self._radar_grid(radius)
        print("Radar grid drawn")

        # Draw the rotating bar
        # self._start_thread(self._rotating_bar, args=(radius,))

        self._center_camera()
        self._scene.set_on_key(
            lambda e:
            (e.key == gui.KeyName.SPACE and self._center_camera() and False) or
            o3d.visualization.gui.SceneWidget.EventCallbackResult.HANDLED
        )
        print("Camera centered")

        self._pcd = o3d.geometry.PointCloud(
            o3d.utility.Vector3dVector([(0, 0, 0)] * 180))
        self._scene.scene.add_geometry(
            self._pcd_name, self._pcd, self.material)
        print("Added sample row")

        def add_point_radar(point):
            add_points_radar([point])

        def add_points_radar(points):
            #print("Adding radar points")
            #for pan, tilt, distance in points:
            #    print(f"pan: {pan}, tilt: {tilt}, distance: {distance}")

            indexes = map(lambda point: int(point[0]) - 1, points)
            points = map(
                lambda point: (
                    self._clamp(point[0], 0, 180),
                    self._clamp(point[2]/500, 0, 6000)
                ),
                points
            )
            points = map(lambda point: (*self._rect(*point),
                         initial_height/2 - 0.01), points)
            points = zip(indexes, points)
            points = list(points)
            print(f"Computed {points}")

            for index, point in points:
                self._pcd.points[index] = point
            # print(f"pcd.points[{index}] = {self._pcd.points[index]} ({point})")

            def update_points():
                self._scene.scene.remove_geometry(self._pcd_name)
                self._scene.scene.add_geometry(
                    self._pcd_name, self._pcd, self.material)

            o3d.visualization.gui.Application.instance.post_to_main_thread(
                self.window, update_points)

        self.add_points = add_points_radar
        self.add_point = add_point_radar
        print("Functions hooked")

        # TODO: remove this in production
        # self._start_thread(self._test_add_points_radar)


def main():
    ports = [port.device for port in list_ports.comports()]
    if not len(ports):
        print("No serial ports found, exiting.")
        return
    # We need to initialize the application, which finds the necessary shaders
    # for rendering and prepares the cross-platform window abstraction.
    gui.Application.instance.initialize()

    ports.reverse()
    w = AppWindow(1024, 768, ports)

    # Run the event loop. This will not return until the last window is closed.
    gui.Application.instance.run()


if __name__ == "__main__":
    main()
